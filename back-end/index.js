const app = require('./src/app');
const PORT = process.env.PORT || 4000;

app.listen(PORT,() => {
    console.log('El servidor inicio en el puerto =>', PORT);
});
