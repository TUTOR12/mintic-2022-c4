const {Router} = require('express');
const controller = require('../controllers/libros')
const router = Router();
const comentarios = require('./comentarios')
const {hasRol} = require('../middlewares/auth')
const comentarioCtrl = require('../controllers/comentarios')

// router.get('/all', (req,res) => {
//     res.json({message:'Libros'})
// })

router.post('/',hasRol(['ADMIN']),controller.create)
router.get('/',controller.getAll)
router.get('/all/comentario',comentarioCtrl.getAll)
router.use('/:libroId/comentario',comentarios.router)
router.get('/:id',controller.getById)
router.put('/:id',hasRol(['ADMIN']),controller.updateLibro)
router.delete('/:id',hasRol(['ADMIN']),controller.deleteLibro)


module.exports = {router}