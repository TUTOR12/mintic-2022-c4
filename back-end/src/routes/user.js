const {Router} = require('express');

const controller = require('../controllers/user');
const router = Router();

router.post('/registrar', controller.registrarse);
router.post('/login',controller.auth);

module.exports = {router};


