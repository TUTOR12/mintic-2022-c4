const {Router} = require('express');
const controller = require('../controllers/comentarios')
const router = Router({mergeParams: true});


router.post('/',controller.create)
router.get('/',controller.getAllBook)
router.get('/:id',controller.get)
router.put('/:id',controller.update)
router.delete('/:id',controller.remove)

module.exports = {router}