const {Router} = require('express');
const {auth} = require('../middlewares/auth')
const router = Router();

const user = require('./user');
const libros = require('./libros');

router.use('/user',user.router);
router.use('/libro',auth,libros.router);


module.exports = {router};