const mongoose = require('mongoose');

const URL = 'mongodb://127.0.0.1:27017/sprint2';

mongoose.connect(URL).
    then(() => {
        console.log('Se conecto a la base de datos');
    }).
    catch((err) => {
        console.log('Se presento el error',err);
    });