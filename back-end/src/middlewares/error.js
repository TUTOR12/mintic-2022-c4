/**
 * 
 * Middleware para responder con un error HTTP
 * 
 * @param {Error | Any} err el error http
 * @param {Request} req request
 * @param {Response} res response
 * @param {Next} next next function
 */
function errorHandler(
    err,
    req,
    res,
    next
  ) {
    res.status(err.statusCode || 500);
    res.json({
      message: err?.message,
      statusCode: err?.statusCode || 500,
    });
  }

module.exports = errorHandler