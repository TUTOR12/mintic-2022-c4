const { HttpError401, responseError } = require('../utils/errors')
const UserModel = require('../models/user');
const moment = require('moment')
const jwt = require('jwt-simple');
const secret = 'xxx';
const tokenLife = 4

/**
 * Middleware para validar el bearer token
 * 
 * @param {Request} req request
 * @param {Response} res response
 * @param {Next} next next function
 */
async function auth(req, res, next) {
    const header = req.get('authorization')
    try {
        if (!header) {
            throw new HttpError401('Not found token')

        }
        const value = header.split(' ')
        if (value && !value.length) {
            throw new HttpError401('Not found token')
        }
        const token = value[1]
        const decoded = jwt.decode(token, secret);
        const user = await UserModel.findById(decoded.sub)
        if (!user) {
            throw new HttpError401()
        }
        const date = moment(decoded.exp)
        const currentDate = moment(Date.now())
        const isExpire = currentDate.isAfter(date)
        if(isExpire){
            throw new HttpError401('El token ha expirado')
        }
        req.user = user;
        next();

    } catch (err) {
        responseError(err, next)
    }


}
/**
 * Generar el token de autenticación
 * 
 * @param {*} sub el usuario del token
 * @returns {String} el token
 */
function generateToken(sub) {
    const currentDate = Date.now()
    const payload = jwt.encode(
        {
            sub,
            exp: moment(currentDate).add(tokenLife,'h').valueOf()
        },
        secret
        );
    return payload
}


function hasRol(roles) {
    const lowerRoles = roles.map(rol => {
        return rol.toLowerCase();
      });
    return function(req, res, next) {
        try {
            const {user} = req
            const tieneRol = user.roles.some(rol => lowerRoles.includes(rol.toLowerCase()))
            if(!tieneRol) {
                throw new HttpError401('No tienes suficientes permisos para esta acción')
            }
            next()
        } catch (err) {
            responseError(err, next)
        }
    }
}

module.exports = {
    auth,
    generateToken,
    hasRol
}