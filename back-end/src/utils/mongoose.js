const {Types} = require('mongoose')
const objectId = (id) => Types.ObjectId(id)


module.exports = {
    objectId
}