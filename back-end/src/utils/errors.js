/**
 *  Clase base para los errores HTTP
 */
class HttpError extends Error {
    /**
     * 
     * @param {Error | any} error el mensage del error
     * @param {Number} statusCode el estado http del error
     */
    constructor(error, statusCode = 500) {
      const formatErrorMessage = error.message || error;
      super(formatErrorMessage);
      this.message = formatErrorMessage
      this.statusCode = statusCode > 400 ? statusCode : 500
    }
}
/**
 * Bad request 
 */
class HttpError400 extends HttpError {
    constructor(error = 'Bad request') {
        super(error, 400)
    }
}
/**
 * Unauthorized
 */
class HttpError401 extends HttpError {
    constructor(error = 'Unauthorized') {
        super(error, 401)
    }
}
/**
 * Forbidden
 */
class HttpError403 extends HttpError {
    constructor(error = 'Forbidden') {
        super(error, 403)
    }
}
/**
 * Not Found
 */
class HttpError404 extends HttpError {
    constructor(error = 'Not Found') {
        super(error, 404)
    }
}

/**
 * 
 * finalizar end point con error http
 * 
 * @param {Error | Any} err el error
 * @param {Callback} next 
 */
function responseError(err, next) {
    if (err instanceof HttpError) {
        next(err)
    } else {
        next(
            new HttpError("Internal server error")
        )
    }
}
module.exports = {
    HttpError,
    HttpError400,
    HttpError401,
    HttpError403,
    HttpError404,
    responseError
}