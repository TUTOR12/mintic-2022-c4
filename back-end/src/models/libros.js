const mongoose = require('mongoose');

const libroSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    autor: {
        nombre: String,
    },
    etiquetas: [String],
    comentarios: [{
        puntos: {
            type: Number,
            default: 3
        },
        comentario: String,
        autor: {type: mongoose.Schema.Types.ObjectId, ref:'user'}
    }],
    created_at: {type: Date, default: Date.now }
});

const libroModel = mongoose.model('libro',libroSchema);

module.exports = libroModel;