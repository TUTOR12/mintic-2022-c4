const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    created_at: {type: Date, default: Date.now },
    roles: [String]
});
userSchema.pre('save', function(next){
    bcrypt.hash(this.password,saltRounds, (err, hash) => {
        this.password = hash;
        next();
    })
});

userSchema.methods.comparePassword = function(candidatePassword, cb){
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
}
const userModel = mongoose.model('user',userSchema);

module.exports = userModel;