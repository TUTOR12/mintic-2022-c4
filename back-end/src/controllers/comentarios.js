const { Error: MongooseError } = require('mongoose')
const LibrosModel = require('../models/libros');
const { HttpError400, HttpError404, responseError } = require('../utils/errors')

const {objectId} = require('../utils/mongoose')

async function create(req, res, next) {
    try {
        const {libroId} = req.params
        if (!libroId) {
            throw new HttpError404('Libro no encontrado')
        }
        await LibrosModel.findByIdAndUpdate(libroId, {
            $push: {
                comentarios: {
                    ...req.body,
                    autor: req.user._id
                }
            }
        })
        res.status(201)
        res.json({message: 'Comentario creado', statusCode:201})
    } catch (err) {
        if (err instanceof MongooseError.ValidationError) {
            responseError(
                new HttpError400(err),
                next
            )
        } else {
            responseError(err, next)
        }
    }

}

async function get(req, res, next) {
    try {
        const {libroId} = req.params
        const {id} = req.params
        if (!libroId) {
            throw new HttpError404('Libro no encontrado')
        }
        if (!id) {
            throw new HttpError404('Comentario no encontrado')
        }
        const comentario = await LibrosModel.aggregate([
            {$match: {
                _id:objectId(libroId),
                'comentarios._id': objectId(id)
            }},
            { "$unwind": "$comentarios" },
            { $project: {
                _id: '$comentarios._id',
                puntos: '$comentarios.puntos',
                comentario: '$comentarios.comentario',
                autor: '$comentarios.autor',
                libro_id: '$_id',
                libro: '$nombre'

            }},
            { $limit: 1 }
        ])
        if(!comentario.length) {
            throw new HttpError404('Comentario no encontrado')
        }
        res.status(200)
        res.json({data:comentario[0], statusCode:200})
    } catch (err) {
        responseError(err, next)
    }

}

async function update(req, res, next) {
    try {
        const {libroId} = req.params
        const {id} = req.params
        if (!libroId) {
            throw new HttpError404('Libro no encontrado')
        }
        if (!id) {
            throw new HttpError404('Comentario no encontrado')
        }
        const {body} = req
        let updateComentario = {}
        Object.keys(body).forEach(key => {
            if(key === '_id') {
                updateComentario[`comentarios.$[elem].${key}`] = objectId(body[key])
            } else {
                updateComentario[`comentarios.$[elem].${key}`] = body[key]
            }
            
        })
        await LibrosModel.findByIdAndUpdate(libroId, {
            $set: {...updateComentario}
        },{
            arrayFilters: [
                {
                    "elem._id": objectId(id)
                }
            ]
        })
        res.status(200)
        res.json({message: 'Comentario actualizado', statusCode:200})
    } catch (err) {
        console.log(err)
        responseError(err, next)
    }
}

async function remove(req, res, next) {
    try {
        const {libroId} = req.params
        const {id} = req.params
        if (!libroId) {
            throw new HttpError404('Libro no encontrado')
        }
        if (!id) {
            throw new HttpError404('Comentario no encontrado')
        }
        await LibrosModel.findByIdAndUpdate(libroId, {
            $pull: {
                comentarios: {
                    _id: id
                }
            }
        })
        res.status(200)
        res.json({message: 'Comentario eliminado', statusCode:200})
    } catch (err) {
        responseError(err, next)
    }
}

async function getAllBook(req, res, next) {
    try {
        const {libroId} = req.params
        if (!libroId) {
            throw new HttpError404('Libro no encontrado')
        }
        const comentario = await LibrosModel.aggregate([
            { "$unwind": "$comentarios" },
            {$match: {
                _id:objectId(libroId),
                'comentarios.autor': req.user._id
            }},
            
            { $project: {
                _id: '$comentarios._id',
                puntos: '$comentarios.puntos',
                comentario: '$comentarios.comentario',
                autor: '$comentarios.autor',
                libro_id: '$_id',
                libro: '$nombre'

            }}
        ]).exec()
        res.status(200)
        res.json({data:comentario, statusCode:200})
    } catch (err) {
        responseError(err, next)
    }
}

async function getAll(req, res, next) {
    try {
        const comentario = await LibrosModel.aggregate([
            { "$unwind": "$comentarios" },
            {$match: {
                'comentarios.autor': req.user._id
            }},
            
            { $project: {
                _id: '$comentarios._id',
                puntos: '$comentarios.puntos',
                comentario: '$comentarios.comentario',
                autor: '$comentarios.autor',
                libro_id: '$_id',
                libro: '$nombre'

            }}
        ]).exec()
        res.status(200)
        res.json({data:comentario, statusCode:200})
    } catch (err) {
        responseError(err, next)
    }
}

module.exports = {
    create,
    get,
    update,
    remove,
    getAllBook,
    getAll,
}