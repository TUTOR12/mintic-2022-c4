const UserModel = require('../models/user');
const { Error: MongooseError } = require('mongoose')
const { generateToken } = require('../middlewares/auth')
const { HttpError401, HttpError400, responseError } = require('../utils/errors')

async function registrarse(req, res, next) {
    try {


        const user = new UserModel(req.body)
        user.roles = ['USER']
        await user.save()

        res.status(201)
        res.json({
            message: 'Usuario creado',
            statusCode: 201
        })
    } catch (err) {
        if (err instanceof MongooseError.ValidationError) {
            responseError(
                new HttpError400(err),
                next
            )
        } else {
            responseError(err, next)
        }
    }
}

async function auth(req, res, next) {
    const body = req.body;
    const user = await UserModel.findOne({ username: body.username })
    user.comparePassword(body.password, function (err, isMatch) {
        try {
            if (!isMatch || err) {
                throw new HttpError401('Wrong credentials')
            } else {
                const payload = generateToken(user._id);
                res.json({ token: payload, statusCode: 200 })
            }

        } catch (err) {
            responseError(err, next)
        }


    })
}


module.exports = {
    registrarse,
    auth,
};