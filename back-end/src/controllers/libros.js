const { Error: MongooseError } = require('mongoose')
const LibrosModel = require('../models/libros');
const { HttpError400, HttpError404, responseError } = require('../utils/errors')

async function create(req, res, next) {
    try {
        const libro = new LibrosModel(req.body)
        await libro.save()
        res.status(201)
        res.json({message: 'Libro creado', statusCode:201})
    } catch (err) {
        if (err instanceof MongooseError.ValidationError) {
            responseError(
                new HttpError400(err),
                next
            )
        } else {
            responseError(err, next)
        }
    }

}

async function getById(req, res, next) {
    try {
        
        const { id } = req.params
        if (!id) {
            throw new HttpError404('Libro no encontrado')
        }
        const libro = await LibrosModel.findById(id).populate({
            path: 'comentarios.autor',
        })
        if(!libro) {
            throw new HttpError404('Libro no encontrado')
        }
        res.json({data: libro, statusCode:200})
    } catch (err) {
        responseError(err, next)
    }

}

async function getAll(req, res, next) {
    try {
        const libros = await LibrosModel.find()
        res.json({data: libros, statusCode: 200})
    } catch (err) {
        responseError(err, next)
    }
}

async function updateLibro(req, res, next) {
    try {
        const {id} = req.params
        const {body} = req
        if(body?.comentarios) {
            delete body.comentarios
        }
        if (!id) {
            throw new HttpError404('Libro no encontrado')
        }
        const libro = await LibrosModel.findByIdAndUpdate(id,body,{new: true})
        if(!libro) {
            throw new HttpError404('Libro no encontrado')
        }
        res.status(200)
        res.json({data:libro, statusCode:200})
    } catch (err) {
        responseError(err, next)
    }
}

async function deleteLibro(req, res, next) {
    try {
        const {id} = req.params
        const {body} = req.body
        if(body?.comentarios) {
            delete body.comentarios
        }
        if (!id) {
            throw new HttpError404('Libro no encontrado')
        }
        const libro = await LibrosModel.findByIdAndRemove(id,body)
        if(!libro) {
            throw new HttpError404('Libro no encontrado')
        }
        res.status(200)
        res.json({message:'Libro eliminado', statusCode:200})
    } catch (err) {
        responseError(err, next)
    }
}

module.exports = {
    create,
    getById,
    getAll,
    updateLibro,
    deleteLibro,
}