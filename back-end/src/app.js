const express = require('express');
const routes = require('./routes/index');
const db = require('./config/db');
const errorHandler = require('./middlewares/error')
const app = express();
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  }));
app.use((req, res, next) =>{
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers",
    "Origin, X-Requeted-With, Content-Type, Accept, Authorization, RBR");
  if (req.headers.origin) {
    res.header('Access-Control-Allow-Origin', req.headers.origin);
  }
  if (req.method === 'OPTIONS') {
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE");
    return res.status(200).json({});
  }
  next();
  });
app.use('/api',routes.router);
app.use(errorHandler)
module.exports = app;