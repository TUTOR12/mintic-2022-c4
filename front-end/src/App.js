import React, {Fragment} from 'react';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import CrearCuenta from './paginas/auth/CrearCuenta';
import Login from './paginas/auth/Login';
import Home from './paginas/Home';
import Libros from './paginas/libros/Libros';
import LibroCrear from './paginas/libros/LibroCrear';
import LibrosEditar from './paginas/libros/LibrosEditar';
import Comentarios from './paginas/libros/Comentarios';
import ComentariosCrear from './paginas/libros/ComentariosCrear';
import ComentariosEditar from './paginas/libros/ComentariosEditar';


function App() {
  return (
    <Fragment>
      <Router>
        <Routes>
          <Route path="/" exact element={<Login/>}/>
          <Route path="/crear-cuenta" exact element={<CrearCuenta/>}/>
          <Route path="/home" exact element={<Home/>}/>
          <Route path="/libros" exact element={<Libros/>}/>
          <Route path="/libro-crear" exact element={<LibroCrear/>}/>
          <Route path="/libro-editar/:idlibro" exact element={<LibrosEditar/>}/>
          <Route path="/comentarios/:idlibro" exact element={<Comentarios/>}/>
          <Route path="/comentarios-crear/:idlibro" exact element={<ComentariosCrear/>}/>
          <Route path="/comentarios-editar/:idlibro" exact element={<ComentariosEditar/>}/>
        </Routes>
      </Router>
    </Fragment>
  );
}

export default App;
