import React, { useState, useEffect } from 'react';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import { useNavigate, useParams } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
import swal from 'sweetalert';

const ComentariosEditar = () => {

    const navigate = useNavigate();

    const [comentarios, setComentarios] = useState({
        comentario: '',
        puntos:0
    });

    const { comentario, puntos } = comentarios;

    const { idlibro } = useParams();
    const [id, libro_id,nombreLibro] = idlibro.split('@');
    const tituloPagina = `Actualización de Comentario: ${nombreLibro}`;
    const loadData = () => {
        APIInvoke.invokeGET(`/api/libro/${libro_id}/comentario/${id}`)
            .then(response => {
                console.log(response)
                setComentarios({...response.data})
            })
    }
    useEffect(() => {
        loadData()
        document.getElementById("comentario").focus();
    }, [])

    const onChange = (e) => {
        setComentarios({
            ...comentarios,
            [e.target.name]: e.target.value
        })
    }

    const actualizarComentario = async () => {

        const data = {
            comentario: comentarios.comentario,
            puntos: comentarios.puntos
        }

        const response = await APIInvoke.invokePUT(`/api/libro/${libro_id}/comentario/${id}`, data)

        if (response.statusCode !== 200) {
            const msg = "El comentario no fue creado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        } else {
            navigate(`/comentarios/${libro_id}@${nombreLibro}`);
            const msg = "El comentario fue creado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
        }

    }

    const onSubmit = (e) => {
        e.preventDefault();
        actualizarComentario();
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={tituloPagina}
                    breadCrumb1={"Listado de Comentarios"}
                    breadCrumb2={"Creación"}
                    ruta1={`/comentarios/${id}`}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">

                            <form onSubmit={onSubmit}>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="comentario">Comentario</label>
                                        <input type="text"
                                            className="form-control"
                                            id="comentario"
                                            name="comentario"
                                            placeholder="Ingrese el comentario del libro"
                                            value={comentario}
                                            onChange={onChange}
                                            required
                                        />
                                        <label htmlFor="puntos">Calificación</label>
                                        <input type="number"
                                            className="form-control"
                                            id="puntos"
                                            name="puntos"
                                            placeholder="Ingrese la calificación"
                                            value={puntos}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Actualizar</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default ComentariosEditar;