import React, { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import APIInvoke from '../../utils/APIInvoke'
import swal from 'sweetalert';


const ComentariosAdmin = () => {

    const [comentarios, setComentarios] = useState([]);

    const { idlibro } = useParams();

    const [id, libro] = idlibro.split('@')

    const cargarComentarios = async () => {
        const response = await APIInvoke.invokeGET(`/api/libro/${id}/comentario/`);
        console.log(response);
        setComentarios(response.data);
    }

    useEffect(() => {
        cargarComentarios()
    }, [])

    const eliminarTarea = async (e, idComentario, idLibro) => {
        e.preventDefault();
        const response = await APIInvoke.invokeDELETE(`/api/libro/${idLibro}/comentario/${idComentario}`);

        if (response.statusCode === 200) {
            const msg = "La tares fue borrada correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
            cargarComentarios();
        } else {
            const msg = "La tarea no fue borrada correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        }

    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={libro}
                    breadCrumb1={"Listado de Libros"}
                    breadCrumb2={"Comentarios"}
                    ruta1={"/libros"}
                />

                <section className="content">

                    <div className="card">
                        <div className="card-header">
                            <h3 className="card-title"><Link to={`/comentarios-crear/${id}@${libro}`} className="btn btn-block btn-primary btn-sm">Crear Comentario</Link></h3>
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">
                            <table className="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style={{ width: '10%' }}>Id</th>
                                        <th style={{ width: '10%' }}>Puntos</th>
                                        <th style={{ width: '65%' }}>Comentario</th>
                                        <th style={{ width: '15%' }}>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        comentarios.map(
                                            item =>
                                                <tr key={item._id}>
                                                    <td>{item._id}</td>
                                                    <td>{item.puntos}</td>
                                                    <td>{item.comentario}</td>
                                                    <td>
                                                        <Link to={`/comentarios-editar/${item._id}@${item.libro_id}@${libro}`} className="btn btn-sm btn-primary">Editar</Link>&nbsp;&nbsp;
                                                        <button onClick={(e) => eliminarTarea(e, item._id, item.libro_id)} className="btn btn-sm btn-danger">Borrar</button>
                                                    </td>
                                                </tr>
                                        )
                                    }

                                </tbody>
                            </table>

                        </div>
                    </div>

                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default ComentariosAdmin;