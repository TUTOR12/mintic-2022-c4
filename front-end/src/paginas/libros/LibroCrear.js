import React, { useState, useEffect } from 'react';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import { useNavigate } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
import { WithContext as ReactTags } from 'react-tag-input';
import swal from 'sweetalert';
const KeyCodes = {
    comma: 188,
    enter: 13
  };
  
const delimiters = [KeyCodes.comma, KeyCodes.enter];
const LibroCrear = () => {

    const navigate = useNavigate();

    const [libro, setLibro] = useState({
        nombre: '',
        autor: ''
    });
    const [tags, setTags] = useState([])
    const handleAddition = tag => {
        setTags([...tags, tag]);
      };
      const handleDrag = (tag, currPos, newPos) => {
        const newTags = tags.slice();
    
        newTags.splice(currPos, 1);
        newTags.splice(newPos, 0, tag);
    
        // re-render
        setTags(newTags);
      };
      const handleDelete = i => {
        setTags(tags.filter((tag, index) => index !== i));
      };
    const { nombre, autor } = libro;

    useEffect(() => {
        document.getElementById("nombre").focus();
    }, [])

    const onChange = (e) => {
        setLibro({
            ...libro,
            [e.target.name]: e.target.value
        })
    }

    const crearLibro = async () => {
        console.log(tags)
        const data = {
            nombre: libro.nombre,
            autor:{
                nombre:libro.autor
            },
            etiquetas:tags.map(tag => tag.id)
        }

        const response = await APIInvoke.invokePOST(`/api/libro/`, data);
        console.log(tags)
        console.log(response)
        if (response.statusCode !== 201) {
            const msg = "El libro no fue creado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        } else {
            navigate("/libros");
            const msg = "El Libro fue creado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });

            setLibro({
                nombre: '',
                autor: ''
            })
            setTags([])
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        crearLibro();
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={"Creación de libros"}
                    breadCrumb1={"Listado de libros"}
                    breadCrumb2={"Creación"}
                    ruta1={"/libros"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">

                            <form onSubmit={onSubmit}>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Nombre</label>
                                        <input type="text"
                                            className="form-control"
                                            id="nombre"
                                            name="nombre"
                                            placeholder="Ingrese el nombre del libro"
                                            value={nombre}
                                            onChange={onChange}
                                            required
                                        />
                                        <label htmlFor="autor">Autor</label>
                                        <input type="text"
                                            className="form-control"
                                            id="autor"
                                            name="autor"
                                            placeholder="Ingrese el autor"
                                            value={autor}
                                            onChange={onChange}
                                            required
                                        />
                                        <label htmlFor="etiquetas">Etiquetas</label>
                                        <ReactTags
                                        id="etiquetas"
                                            className="form-control"
                                            tags={tags}
                                            delimiters={delimiters}
                                            handleDelete={handleDelete}
                                            handleAddition={handleAddition}
                                            handleDrag={handleDrag}
                                            inputFieldPosition="bottom"
                                            autocomplete
                                        />
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Crear</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default LibroCrear;